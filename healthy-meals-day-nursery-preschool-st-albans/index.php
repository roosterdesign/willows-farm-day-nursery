<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Healthy meals at Day Nursery and Preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="As you would expect from Willows Farm Day Nursery and PreSchool, we serve  healthly and nutrition meals within the nursery fees.  Another reason to choose this St Albans Nursery and Preschool."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="nutrition">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Diet and Nutrition at Willows Farm Day Nursery and Pre-School</h1>
        				<div class="divide head1 red"></div>
        				<p>Breakfasts, lunches and meals served at Willows Farm Day Nursery and Pre-School are healthy and wholesome and included in your fees.</p>
        				<p>Freshly prepared in our onsite kitchen, we can easily adapt menus to include any child’s special dietary needs.  We don’t add sugar or salt to our dishes, which are created to ensure your child receives balanced and nutritious meals.</p>
        				<h2>Plenty of variety at the day nursery and pre-school</h2>
        				<p>Our day nursery and pre-school menus are rolled over a four-week period and copies are available for parents to help plan their own meals at home.</p>
        				<p>To see a sample of our daily menus <a href="?" title="View our Sample Menus">click here</a>.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
        			<? include("../assets/inc/contact-signpost.php") ?>
        			        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>