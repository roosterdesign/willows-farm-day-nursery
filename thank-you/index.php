<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Thank You | Willows Farm Day Nursery and Pre-School</title>
        <meta name = "description" content="A new Day Nursery and Pre-school at the award-winning Willows Farm Village in St Albans, Hertfordshire.  Ofsted registered childcare for babies from 6 weeks to children aged 5 years."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">

        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body>

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Thank You</h1>
        				<div class="divide head1 red"></div>
        				<p>Thank you for your enquiry. We will be in touch soon.</p>
        				<p><a href="/" title="Willows Farm Day Nursery and Pre-School">Click here</a> to return to our homepage.</p>
        			</div>
        			
        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			<? include("../assets/inc/contact-signpost.php") ?>
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->
        
    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>