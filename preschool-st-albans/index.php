<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Preschool  Day Nursery in St Albans | Willows Farm</title>
        <meta name = "description" content="Willows Farm Preschool near St Albans ensure children are ready for mainstream school and very much enjoy their introduction to education.  Early years foundation stage followed including the seven areas of learning."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="preschool">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<hgroup>
        				 <h1>Pre-School</h1>
        				 <h2>From 3 to 5 Years (Dependant on individual child’s needs)<br>Ratio 1:8</h2>
        				</hgroup>
        				<div class="divide head1 red"></div>
        				<p>Our aim within the Pre-School is to ensure that your child is ready and eager to start mainstream school.</p>
        				<p>Following the key areas of the Early Years Foundation Stage and led by qualified practitioners, the structured programme of activities is rich and varied including music, science experiments, creative play and gardening. The childcare provided is loaded with learning experiences.</p>
        				<p>The type of teaching is enormously varied. We combine both adult led and child-initiated activities to encourage independent learning and motivate children to link together the areas of development.</p>
        				<h2>Full access to Willows Farm for pre-school children</h2>
        				<p>A lot of learning can also take place when visiting Willows Farm Village, understanding the seasonal changes on a farm, seeing baby animals, watching vegetables grow in the growing patch and counting how many times they ride the slides in Woolly Jumpers!</p>
        				<h3>Private pre-school garden</h3>
        				<p>In addition to all the activities within Willows  Farm, pre-school children have their own private garden adjacent to their pre-school Room; giving them the best of both worlds.</p>
        				<h4>Regular pre-school updates on your child’s progress</h4>
        				<p>We also help parents continue with their child’s learning at home through contact with your key person and an updated learning journal.</p>
        				<p>As the children finish their time in Pre-School with us we celebrate your child moving onto mainstream school in style: with a fun and exciting graduation ceremony. An occasion for all the family with a graduation service (complete with little gowns and hats), a children's concert, BBQ and a children's entertainer.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
        			<? include("../assets/inc/contact-signpost.php") ?>
        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->
        
    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>