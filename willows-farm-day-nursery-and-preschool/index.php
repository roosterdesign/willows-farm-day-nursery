<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Willows Farm Village Day Nursery and preschool in St Albans</title>
        <meta name = "description" content="Children and babies who attend Willows Farm Day Nursery and Preschool have full access to all the facilities at Willows Farm Village.  Great childcare in the St Albans countryside."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="willowsfarmvillage">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	
           	<!-- Header -->
           	<? include("../assets/inc/header.php") ?>
           	<!-- Header -->

        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>An Ideal Location for a Nursery and Pre-School</h1>
        				<div class="divide head1 red"></div>
        				<p>Having brought up their family in a countryside environment, Willows farmers Andrew and Anna were struck by the opportunity a Day Nursery at Willows Farm offered.</p>
        				<p>Anna says, "I believe giving children the opportunity to experience the countryside and its animals is significant in their development. An appreciation of where the food they eat comes from and the care of animals and other living beings is so important when growing up."</p>
        				<p>Andrew adds "Having worked with Kids Play Childcare in offering our Activity Day Camps during the school holidays, I knew we’d found the partner who shared our ethos of offering the highest standards of professional care."</p>
        				<h2>Full farm access for day nursery and pre-school children</h2>
        				<p>Children who attend Willows Farm Day Nursery and Pre-School have full access to all the outdoor and under cover facilities at Willows Farm Village, under the supervision of the nursery staff.</p>
        				<p>This includes all the seasonal events that are put on: from lambing in February, Easter Egg Hunts in the spring, an A-maize-ing Maze in the summer, potato harvesting in September, Pumpkin Picking in the autumn. Not forgetting our festive activities including a synthetic ice rink and a ride into the festive wood during our Santa event.</p>
        				<p>For more information on Willows Farm Village visit <a href="http://www.willowsfarmvillage.com" title="Willows Farm Village" target="_blank">www.willowsfarmvillage.com</a>.</p>
        				<p>As a member of Willows Farm Village you receive special discounts on the Day Nursery and Pre-School fees – <a href="/fees-and-grants-day-nursery-preschool-st-albans" title="Willows Farm Nursery Fees and Grants">click here for more details</a>.</p>
        				<p>Due to the excellent road network, Willows Farm Village Day Nursery and Pre-School is easy to reach from Hertfordshire and North London, including London Colney, St Albans, Watford and Radlett.</p>
        			</div>

        			
        			<!-- Aside -->
        			<? include("../assets/inc/aside.php") ?>
        			<!-- /Aside -->
        			
        			
        			<div class="clearfix"></div>

        			<!-- Contact signpost -->
		        	<? include("../assets/inc/contact-signpost.php") ?>
        			<!-- /Contact signpost -->

        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	
        	<!-- Footer -->
        	<? include("../assets/inc/footer.php") ?>
        	<!-- /Footer -->
        	
                
        </div>
        <!-- /Page Wrapper -->
        

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>