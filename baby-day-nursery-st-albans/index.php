<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Baby Day Nursery in St Albans | Willows Farm</title>
        <meta name = "description" content="Using movement, sound, colour and touch, babies are encouraged to explore, develop and enjoy their second home at Willows Farm Day Nursery near St Albans."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="babies">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<hgroup>
        				 <h1>Day Nursery - Baby and Pre-Toddler Rooms</h1>
        				 <h2>From 6 weeks to 24 months (Dependant on individual child’s needs)<br>Ratio 1:3</h2>
        				</hgroup>
        				<div class="divide head1 red"></div>
        				<p>We have created a safe, caring and stimulating environment in our purpose-built Baby and Pre-Toddler Rooms.  Using movement, sound, colour and touch, babies are encouraged to explore, develop and above all, enjoy their 'second home'.  Outdoors, your child will also enjoy a dedicated baby area in our gardens and visits to the Farm Village to enjoy the farmyard animals, and age appropriate adventure play.</p>
        				<h2>A caring day nursery team</h2>
        				<p>We strive to mirror the care routines you give your child; through regular contact with your key person we can help ensure your child’s feeding, nappy changes and sleep routines are synchronised with those you use at home.</p>
        				<p>Your child will also benefit from freshly prepared, <a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">healthy nursery food</a> and milk bottle feeds made by a senior member of staff.</p>
        				<p>When they are ready, babies progress to the Pre-Toddler Rooms and then move up through the Nursery’s <a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery in St Albans">Toddler Rooms</a> and finally into <a href="/preschool-st-albans" title="Pre-School in St Albans">Pre-School</a>.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>

        			<? include("../assets/inc/contact-signpost.php") ?>

        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	
        	<!-- Footer -->
        	<? include("../assets/inc/footer.php") ?>
        	<!-- /Footer -->
        	
        </div>
        <!-- /Page Wrapper -->
        
    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>