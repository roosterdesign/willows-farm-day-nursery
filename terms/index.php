<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Terms and Conditions | Willows Farm Day Nursery and Pre-School</title>
        <meta name = "description" content="A new Day Nursery and Pre-school at the award-winning Willows Farm Village in St Albans, Hertfordshire.  Ofsted registered childcare for babies from 6 weeks to children aged 5 years.">
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body>

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Terms</h1>
        				<div class="divide head1 red"></div>
        				<p>Willows Farm Day Nursery and Pre-School are totally committed to protecting the privacy of  our site visitors and customers. When you use any Willows Farm Day Nursery and Pre-School site, you  are consenting to the practices set forth in this Privacy Policy.</p>
    <p><strong>Your IP (Internet Provider) Address</strong><br>
      We use your IP address to help diagnose problems with our  server and to administer the Willows Farm Day Nursery and Pre-School database. Your IP address is used  to help identify you and to gather broad demographic information about  you as it is on most other websites. Your IP address may also be used to  assist in the detection of fraud and we may pass this information to  the police if necessary.</p>
    <p><strong>What are Cookies?</strong><br>
      A cookie is a small file that is automatically issued to  your computer when you enter any Willows Farm Day Nursery and Pre-School site, and which the browser on  your computer’s hard drive stores. Cookies identify your computer, not  the individual user of your computer. Cookies store some basic  information that helps us to identify if you have visited any Willows Farm Day Nursery and Pre-School  sites before and if you have provided us with some personalised  information.</p>
    <p>We use cookies to deliver content specific to your  interests, to save your password (if you have supplied us with one) so  you do not have to re-enter it each time you visit any Willows Farm Day Nursery and Pre-School site,  and for other purposes. You can disable cookies or set your browser to  alert you when cookies are being sent, but if you choose to do so you  may not be able to access some areas on a Willows Farm Day Nursery and Pre-School site.</p>
    <p><strong>Information We Request Directly From You</strong><br>
      Site registration forms, order forms, surveys and  competitions require you to give us personal information (like your  name, address and/or email address). You may also be asked to provide  financial information (like your account or credit card numbers). The  information we learn from customers helps us personalise and continually  improve your shopping experience.</p>
    <p><strong>How We Use Your Information</strong><br>
      We will not rent or sell your name, address, e-mail address,  credit card information or personal information. Your personal  information is used by us to contact you by post, phone or by email when  necessary in connection with transactions entered into by you on the  site. We may also send you exclusive Willows Farm Day Nursery and Pre-School emails, which will inform  you about our new products and special offers.</p>
    <p>You will be given the opportunity to unsubscribe from this  service whenever an email is sent to you. If you do not wish to receive  information emails from Willows Farm Day Nursery and Pre-School simply unsubscribe at any. From time to  time, Willows Farm Day Nursery and Pre-School may contact you by post or by email, with information or  offers regarding products, offers or services.</p>
    <p><strong>E-mail</strong><br>
      Messages sent over the Internet cannot be guaranteed to be  completely secure as they can be intercepted, lost or corrupted. We are  therefore not responsible for the accuracy of any messages sent by  e-mail over the Internet - whether from us to you or from you to us.</p>
    <p><strong>Copyright</strong><br>
      The copyright for all the information on each screen and  page of our website is owned by or licensed to Willows Farm Day Nursery and Pre-School, unless we state  that it belongs to someone else. You may copy, download or temporarily  store extracts from our website for your personal use or to help you use  our products or services. You must not alter anything. You cannot use  the information in any other way without our written permission.</p>
    <p><strong>Your queries</strong><br>
      If you have an enquiry or concern with these terms please contact us by phone, email or post.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			<? include("../assets/inc/contact-signpost.php") ?>
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>