<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Visit Day Nursery and Pre-school in St Albans | Willows Farm</title>
        <meta name = "description" content="Arrange to visit the St Albans based Willows Farm Day Nursery and Pre-School or attend an Open Day.  See website for dates of Open Days."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
	</head>
    <body id="tours">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Day Nursery and Pre-School Open Days</h1>
        				<div class="divide head1 red"></div>
        				<p>We regularly arrange Day Nursery and Pre-School Open Days and, in-between these, encourage parents to visit us for a tour.</p>
        				<h2>Attend an Open Day</h2>
        				<p>To ensure you receive an invitation to our next Open Day, please provide your contact details below and we’ll be in touch shortly with the date.</p>
        				

						
						
        				<form method="post" action="../assets/emailer/formmail.php" name="opendayForm" id="opendayForm">
						<input type="hidden" name="env_report" value="REMOTE_HOST,REMOTE_ADDR,HTTP_USER_AGENT,AUTH_TYPE,REMOTE_USER" />
					    <input type="hidden" name="recipients" value="louiseATwillowsfarmdaynursery.com" />
					    <input type="hidden" name="bcc" value="neilATauburn.co.uk"/>
					    <input type="hidden" name="subject" value="Willows Farm Day Nursery - Open Day Enquiry" />
					    <input type="hidden" name="derive_fields" value="email=email,realname=name" />
					    <input type="hidden" name="good_url" value="/thank-you" />
					    <input type="hidden" name="mail_options" value="Exclude=realname;Submit;Submit_x;Submit_y,HTMLTemplate=OpendayEnquiry.htm,TemplateMissing=N/A,NoPlain,KeepLines" />
							<div class="left">
							<label>Your Name:</label>
							<input name="name" id="name" tabindex="1" />
							<label>Your E-mail:</label>
							<input name="email" id="email" tabindex="3" />
							</div>
							                        
							<div class="right">
							<label>Town you live in:</label>
							<input name="town" id="town" tabindex="2" />
							<label>Your Childs Date of Birth:</label>
							<input class="dob date" name="dob_d" id="dob_d" maxlength="2" tabindex="4" /><span>/</span><input class="dob" name="dob_m" id="dob_m" maxlength="2" tabindex="5" /><span>/</span><input class="dob year" name="dob_y" id="dob_y" maxlength="4" tabindex="6" />
							</div>
							
							<input type="submit" name="" class="submit" value="Submit form" />
							<p class="submit-error">Form not submitted, please ensure all required fields are completed correctly and try again.</p>
							<div class="clearfix"></div>
						</form>



        				<h3>Book a tour of the day nursery or pre-school</h3>
        				<p>To arrange a tour of Willows Farm Day Nursery and Preschool  and meet the Manager please complete your details below and we’ll be in touch to confirm your date and time.</p>
        				
        				<form method="post" action="../assets/emailer/formmail.php" name="tourForm" id="tourForm">
						<input type="hidden" name="env_report" value="REMOTE_HOST,REMOTE_ADDR,HTTP_USER_AGENT,AUTH_TYPE,REMOTE_USER" />
					    <input type="hidden" name="recipients" value="louiseATwillowsfarmdaynursery.com" />
					    <input type="hidden" name="bcc" value="neilATauburn.co.uk"/>
					    <input type="hidden" name="subject" value="Willows Farm Day Nursery - Book a Tour Enquiry" />
					    <input type="hidden" name="derive_fields" value="email=email2,realname=name" />
					    <input type="hidden" name="good_url" value="/thank-you" />
					    <input type="hidden" name="mail_options" value="Exclude=realname;Submit;Submit_x;Submit_y,HTMLTemplate=TourEnquiry.htm,TemplateMissing=N/A,NoPlain,KeepLines" />
					    
							<div class="left">
							<label>Your Name:</label>
							<input name="name2" id="name2" tabindex="7" />
							<label>Your preferred date:</label>
							<input name="date" id="date" tabindex="9" /><br>
							</div>
							                        
							<div class="right">
							<label>Your E-mail:</label>
							<input name="email2" id="email2" tabindex="8" />
							<label>Your preferred time:</label>
							<input name="time" id="time" tabindex="10" />
							</div>

							
							<input type="submit" name="" class="submit" value="Submit form" />
							<p class="submit-error">Form not submitted, please ensure all required fields are completed correctly and try again.</p>
							<div class="clearfix"></div>
						</form>
						
						
						<h4>Directions for Willows Farm Day Nursery and Pre-school</h4>
						<p>Willows Farm Day Nursery and Pre-School is located at junction 22 of the M25, close to London Colney, St Albans, Radlett and Watford. For more information, please <a href="/find-day-nursery-preschool-st-albans" title="">click here</a>.</p>

        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>

		        	<? include("../assets/inc/contact-signpost.php") ?>
        		
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
        	
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
        
    </body>
</html>
