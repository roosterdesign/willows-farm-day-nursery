<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>About Day Nursery and Pre-School in St Albans | Willows Farm</title>
        <meta name = "description" content="Willows Farm Day Nursery and Pre-School in St Albans Hertfordshire offers Ofsted registered childcare in a countryside setting.  Caring for babies from 6 weeks up to 5 year old children.">
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="about">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>

        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>About Us</h1>
        				<div class="divide head1 red"></div>
        				<p>Willows Farm Day Nursery and Pre-School is operated jointly with Kids Play Childcare, a company that now employs over 150 staff throughout the South East.</p>
        				<p>Providing Ofsted registered nursery day care, breakfast clubs, after school clubs and holiday activity day camps, we can look after your child’s needs from 6 weeks right through to holiday childcare whilst at school.</p>
        				<p>Extremely proud of their OUTSTANDING OFSTED recognitions, these set the standard across all our childcare settings.</p>
        				<p>For more information on Kids Play childcare visit <a href="http://www.kidsplaychildcare.co.uk" title="Kids Play childcare" target="_blank">www.kidsplaychildcare.co.uk</a>.</p>	
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
		        	<? include("../assets/inc/contact-signpost.php") ?>
        	
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>