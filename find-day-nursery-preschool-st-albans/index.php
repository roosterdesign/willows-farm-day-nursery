<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Find Day Nursery and Preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="Willows Farm Village Day Nursery and Pre-School is just 200 metres from Junction 22 of the M25  in a farm countryside setting, near St Albans, Hertfordshire AL4 0PF."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="location" onload="initialize()">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper" >
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">

        		<section class="layout">
        			<div class="main red">
       				<h1>Easy to reach Day Nursery and Pre-school<br>near St Albans</h1>
       				<div class="divide head1 red"></div>
       				<p>Accessible from all areas throughout Hertfordshire and North London, Willows Farm Day Nursery and Pre-School provides childcare for children from 6 weeks old to 5 years old.</p>

       				<div class="left">
       				  <h2>Address</h2>
       				  <p><strong>Willows Farm Day Nursery and Pre-School</strong><br>Coursers Road,<br>London Colney,<br>St Albans,<br>Hertfordshire,<br>AL4 0PF.</p>
       				</div>
       				
       				<div class="right">
       				  <h3>Day Nursery and Pre-School Manager</h3>
       				  <p><strong>Name:</strong> Louise Hobbs<br><strong>Tel:</strong> 01727 829535<br><strong>Email:</strong> <script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " title='Email us'>" + linktext + "</a>")</script></p>
       				  <p>Please contact us to arrange a visit. Simply call <strong>01727 829535</strong>, email <script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " title='Email us'>" + linktext + "</a>")</script> or <a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">click here to book a visit online</a>.</p>
       				</div>
       				
       				<div class="clearfix"></div>
       				<hr class="dashed-divide">

       				<!-- Map -->
       				<h4>Map</h4>
       				<p>The day nursery and pre-school at Willows Farm Village is easily reached from all over North London and Hertfordshire, plus extremely close to London Colney, St Albans, Watford and Radlett.</p>
					<div id="googlemap">
					<div id="map_holder"></div>
					</div>
					<!-- /Map -->

					</div>
        			
        			<!-- Aside -->
        			<? include("../assets/inc/aside.php") ?>
        			<!-- /Aside -->
        			
        			<div class="clearfix"></div>
        			
        			<? include("../assets/inc/contact-signpost.php") ?>
        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
        	
        </div>
        <!-- /Page Wrapper -->
        
    <? include("../assets/inc/global.js.php") ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSinrEQnqAMDUOWvkt8yr-j2jRL26T318&amp;sensor=false"></script>

    </body>
</html>