<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Privacy Policy | Willows Farm Day Nursery and Pre-School</title>
        <meta name = "description" content="A new Day Nursery and Pre-school at the award-winning Willows Farm Village in St Albans, Hertfordshire.  Ofsted registered childcare for babies from 6 weeks to children aged 5 years."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body>

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>

        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Privacy Policy</h1>
        				<div class="divide head1 red"></div>
        				
						<p>Willows Farm Day Nursery and Pre-School understands the issue regarding privacy on the Internet and  as a result we will explain how we handle the information we receive  through our website.</p>
<p>The technology we use collects information such as domains name,  search engines used and keywords searched on and this information is  collated to identify number of visits to our website and other such  statistics for the purposes of improving our website and our marketing.  This information will not be used for any other purposes.</p>
<p>However, you may wish to provide us with additional information to  allow us to respond to your requests for further contact. Willows Farm Day Nursery and Pre-School will  not provide this information to any companies outside of the group. You  will always be given the opportunity to say that you do not wish to  receive further information.</p>
<p>Any information we collect via either of the methods stated above  will be held for as long as necessary to conduct business relationships.  It will be stored securely and you will have the right to access  information held by us in respect of you, as instructed by Data  Protection legislation.</p>
<p>By using this site, you signify your assent to Willows Farm Day Nursery and Pre-School Privacy  Policy. If you do not agree to this policy, please do not use this site.  We reserve the right, at our discretion, to change modify, add, or  remove portions of this policy at any time. Please check this page  periodically for changes. Your continued use of this site following the  posting of changes to these terms will mean you accept those changes.</p>
<p>If you have any questions or comments contact us via the contact details on our Contact page.</p>
<h4>Do we use cookies?</h4>
<p>Yes. Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information.</p>
<p>We use cookies to compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			<? include("../assets/inc/contact-signpost.php") ?>
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>