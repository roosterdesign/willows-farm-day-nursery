<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Care and Education - Willows Farm Day Nursery</title>
        <meta name="description" content="">
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="education">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<!-- Header -->
           	<? include("../assets/inc/header.php") ?>
           	<!-- Header -->
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Education</h1>
        				<div class="divide head1 red"></div>
        				<p><img src="/assets/img/education.jpg" alt="Care and Education"></p>
        				<p><strong>Select from an age bracket below for more details:</strong></p>
        				
        				<h2 class="top">Babies</h2>
        				<p>From 6 weeks to 24 months (Dependant on individual child’s needs)<br><em>Ratio 1:3</em></p>
        				<p class="arrow"><a href="/baby-day-nursery-st-albans" title="Baby Day Nursery in St Albans">Click here to find out more</a></p>
        				<hr class="dashed-divide">
        				<h2 class="top">Toddlers</h2>
        				<p>From 24 to 36 months (Dependant on individual child’s needs)<br><em>Ratio 1:4</em></p>
        				<p class="arrow"><a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery in St Albans">Click here to find out more</a></p>
        				<hr class="dashed-divide">
        				<h2 class="top">Pre-School</h2>
        				<p>From 3 to 5 Years (Dependant on individual child’s needs)<br><em>Ratio 1:8</em></p>
        				<p class="arrow"><a href="/preschool-st-albans" title="Pre-School in St Albans">Click here to find out more</a></p>
        				<hr class="dashed-divide">
        				<p>Please contact us to arrange a visit. Simply call <strong>01727 829535</strong>, email <script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " title='Email us'>" + linktext + "</a>")</script> or <a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">click here to book a visit online</a>.</p>

        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>

		        	<? include("../assets/inc/contact-signpost.php") ?>

        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
        	
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>