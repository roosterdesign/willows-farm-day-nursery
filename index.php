<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Day Nursery and Pre-School in St Albans - Willows Farm Village</title>
        <meta name = "description" content="A new Day Nursery and Pre-school at the award-winning Willows Farm Village in St Albans, Hertfordshire.  Ofsted registered childcare for babies from 6 weeks to children aged 5 years."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="assets/css/normalize.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <script src="assets/js/vendor/modernizr-2.6.2.min.js"></script>	
        <? include("assets/inc/analytics.php") ?>
    </head>
    <body id="home">
    <?php $homeslider = 'home'; ?>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
        
           <? include("assets/inc/header.php") ?>
        	
        	<!-- Home strip wrapper -->
        	<div id="education-wrapper">
        		<section class="layout">
        			<article class="red">
	        			<header class="clearfix">
	        			  <h1>Babies</h1>
	        			</header>
	        			<figure>
						  <img src="/assets/img/home-education/babies.png" alt="Baby Day Nursery, St Albans">
						  <figcaption>From 6 weeks to 24 months | Ratio 1:3</figcaption>
						</figure>
	        			<p>We have created a safe, caring and stimulating environment in our purpose-built Baby and Pre-Toddler Rooms... <a href="/baby-day-nursery-st-albans" title="Baby Day Nursery, St Albans">read more</a></p>
        			</article>
        			<article class="yellow">
	        			<header class="clearfix">
	        			  <h1>Toddlers</h1>
	        			</header>
	        			<figure>
						  <img src="/assets/img/home-education/toddlers.png" alt="Toddler Day Nursery, St Albans">
						  <figcaption>From 24 months to 36 months | Ratio 1:4</figcaption>
						</figure>
	        			<p>Every activity in the Toddler Rooms is considered a learning opportunity. More structured than the Baby Room, your child will enjoy... <a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery, St Albans">read more</a></p>
        			</article>
        			<article class="blue">
	        			<header class="clearfix">
	        			  <h1>Pre-School</h1>
	        			</header>
	        			<figure>
						  <img src="/assets/img/home-education/pre-school.png" alt="Pre-School, St Albans">
						  <figcaption>From 3 years to 5 years | Ratio 1:8</figcaption>
						</figure>
	        			<p>Our aim within the Pre-School is to ensure that your child is ready and eager to start mainstream school... <a href="/preschool-st-albans" title="Pre-School, St Albans">read more</a></p>
        			</article>
        		</section>
        	</div>
        	<!-- /Home strip wrapper -->
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Welcome to Willows Farm Day Nursery<br>and Pre-School</h1>
        				<div class="divide head1 red"></div>
        				<h2>Childcare in the Countryside, near St Albans</h2>
        				<p>Located at the award-winning Willows Farm Village your child will enjoy the highest standards of childcare in which to develop and gain their early years experience in a stimulating countryside environment – yet only 200 metres from junction 22 of the M25.</p>
        				<img src="/assets/img/home/welcome.jpg" alt="Willows Farm Day Nursery" class="left-img">
        				<p>Working in partnership with Kids Play Childcare, our qualified and caring staff ensure you receive regular feedback from your nominated key person to keep you up to date with your child’s progress.</p>
        				<p>Outstanding recognitions from <a href="/ofsted-report-st-albans-day-nursery-preschool" title="OFSTED report Nursery and Preschool in St Albans">Ofsted</a> have been achieved across the Kids Play Childcare group and this sets the standard for Willows Farm Day Nursery and Pre-School.</p>
        				<p>With freshly prepared <a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">healthy meals</a> made onsite, complimentary coffee for parents and full access to all the activities at <a href="/willows-farm-day-nursery-and-preschool" title="Willows Farm Village Day Nursery and Pre-School">Willows Farm Village</a>, this is an exceptional childcare opportunity.</p>
        				<p>Based at Willows Farm Village, the day nursery and pre-school is easy to reach from all over Hertfordshire and North London, including St Albans, London Colney, Watford and Barnet.</p>
        				<p><a href="/about-day-nursery-and-pre-school-in-st-albans" class="btn" title="About Willows Farm Village Day Nursery and Pre-School">Find out more</a></p>
        			</div>

        			<? include("assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
		        	<? include("assets/inc/contact-signpost.php") ?>
        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->
        
    <? include("assets/inc/global.js.php") ?>
    
    
 <script type="text/javascript" src="assets/js/vendor/jquery.cycle.all.js"></script>
 <script>
 	// Homepage slider
	$('body#home #hero').cycle({
	    timeout: 4500
	});
	
	// resize slideshow when window is resizes
	$(window).resize(function(){
    	$("body#home #hero").width($(window).width());
    	$("body#home #hero div").width($(window).width());
    });
</script>


    </body>
</html>