<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>FAQs for Day Nursery and Preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="All your questions answered for the new  Willows Farm Day Nursery and PreSchool near St Albans, London Colney, Watford or Radlett."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
        <noscript><style>.st-accordion ul li{height:auto;}.st-accordion ul li > a span{visibility:hidden;}</style></noscript>
	</head>
    <body id="faqs">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>FAQ’s for the Nursery and Pre-School</h1>
        				<div class="divide head1 red"></div>
        				
			                <!-- Accordion -->
			        		<div id="st-accordion" class="st-accordion clearfix">
			                  <ul>
			                  	 <li>
			                  	   <a href="#" class="trigger">What’s included in your nursery and pre-school fees?</a>
			                  	   <div class="st-content">
			                  	   	<ul class="tick">
			                  	   	 <li>Trained and dedicated staff offering focused and caring childcare from the moment your child arrives to the time they leave to return home</li>
			                  	   	 <li>Dedicated drop off and pick up areas and parking for our Nursery customers</li>
			                  	   	 <li>Freshly prepared nutritious meals, snacks and drinks – including a hot two-course meal for the toddlers</li>
			                  	   	 <li>Milk for the babies</li>
			                  	   	 <li>Nappies and creams for the babies</li>
			                  	   	 <li>Full access to Willows Farm Village to enjoy all the adventure play and seasonal farm led activities during nursery hours</li>
			                  	   	 <li>Free tea and coffee for parents</li>
			                  	   	 <li>Parents Evenings</li>
			                  	   	 <li>Graduation ceremony event (when they leave for mainstream school)</li>
				                  	</ul>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">I am a Willows Farm Village Member - do I receive a discount on my nursery fees?</a>
			                  	   <div class="st-content">
			                  	     <p>Yes!  Willows Farm Village Members receive a 5% discount off the fees for under 3 year olds and 10% discount off children aged 3 and over.</p>
			                  	     <p>If you are not yet a member we would encourage you to join as the cost of membership will be covered by the discounted fees within the first year and you’ll also have unlimited entry to Willows Farm Village during weekends – a place your children will love and feel comfortable at in their leisure time too.</p>
			                  	     <p>To become a member call 0870 129 9718 or click here where you can download an application form  (<a href="http://www.willowsfarmvillage.com/willowsmembership/" title="Willows Farm Village Membership" target="_blank">www.willowsfarmvillage.com/willowsmembership</a>).</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Tell me about what I have to pay and do I have to pay for holidays and sickness?</a>
			                  	   <div class="st-content">
			                  	     <p>The registration fee is payable to secure your child’s place at Willows Farm Day Nursery and Pre-School and in non-returnable. <a href="/assets/registration-form.pdf" title="View our Registration Pack" target="_blank">Click here to see what’s included in your Registration Pack</a>.</p>
			                  	     <p>A holding deposit is also payable against the first month's fees.</p>
			                  	     <p>All fees are payable monthly in advance by cheque, cash, Childcare vouchers or through your bank by Direct Debit.</p>
			                  	     <p>We are open 52 weeks per year only closing for the statutory Bank Holidays, for which you are NOT charged for childcare. However we do reserve the right to charge for any additional Bank Holidays that are declared. Dates at Christmas may vary.  We do not offer any refunds for holidays or sickness.</p>
			                  	     <p class="arrow"><a href="/fees-and-grants-day-nursery-preschool-st-albans" title="Willows Farm Nursery Fees and Grants">Click here to see our fees</a>.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">What is included in the day nursery and pre-school registration fee?</a>
			                  	   <div class="st-content">
			                  	     <p>Our registration fee covers the following costs and includes a few goodies too!</p>
			                  	     <ul class="tick">
			                  	       <li>Administration of setting up your child on our systems</li>
			                  	       <li>Cost of setting up your monthly Direct Debit (note there is no reduction in  the Registration fee if you choose to pay by another method which also have costs attached)</li>
			                  	       <li>A FREE Willows Farm Day Nursery and Pre-School back-pack for your child</li>
			                  	       <li>Two FREE Willows Farm Day Nursery and Pre-School t-shirts</li>
			                  	       <li>A donation to our nominated charity – on your child’s journey with us we’ll introduce them and encourage an age appropriate understanding and empathy for those less fortunate that ourselves.</li>
			                  	     </ul>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">What ratio of nursery or pre-School staff to children is there?</a>
			                  	   <div class="st-content">
			                  	     <p>In accordance with OFSTED, our ratios at Willows Farm Village Day Nursery and Pre-School, are:</p>
			                  	     <ul class="arrow">
			                  	       <li>0-2yrs 1:3</li>
			                  	       <li>2-3yrs 1:4</li>
			                  	       <li>3-5yrs 1:8 (1:13 for Qualified Level 6 Childcare Practitioners)</li>
			                  	     </ul>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Are your staff qualified and checked for suitability?</a>
			                  	   <div class="st-content">
			                  	     <p>All employees at Willows Farm Day Nursery and Pre-School are subject to an enhanced CRB disclosure and appropriate references are sought. The qualifications amongst our team meet the current welfare requirements. We have staff with degree level qualifications or NVQ Level 3 (or equivalent).</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">How do I know that my child will be secure at your nursery and pre-school?</a>
			                  	   <div class="st-content">
			                  	     <p>Willows Farm Day Nursery and Pre-School is secured at all times using a Digi-Lock System and people are only allowed to enter the building if recognised by the member of staff opening the front door. We also have CCTV cameras at the entrances.</p>
			                  	     <p>If you are unable to collect your child, you must inform a member of staff and the password system would be operated to ensure complete security.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Do the nursery and pre-school children go on outings?</a>
			                  	   <div class="st-content">
			                  	     <p>Yes! We utilise the local area for outings e.g. walks, a trip to the shops or library.  Local trips apply to all age groups; however outings further afield are limited to our older age groups.  In all instances additional parental consent is required and all aspects of the outing/trip are risk assessed.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Will a particular member of staff be responsible for my child?</a>
			                  	   <div class="st-content">
			                  	     <p>Every child at Willows Farm Day Nursery and Pre-School, has their own key person who  gets to closely know the child and their family. This special person will track your child’s development and plan their daily activities, as well as keeping you informed about your child’s day.</p>
			                  	     <p>All our childcare practitioners also work well as a team and are welcoming and approachable.</p>
			                  	     <p>Each child under 3 years has a daily diary; this is your record of your child’s day so we can ensure a smooth transition between home and nursery. Your key person is available to speak with at any time during opening hours  or you can make an appointment to speak with them on a more personal level.</p>
			                  	     <p>Regular parent’s evenings help us to keep you informed of your child’s development.  We invite you to come along and discuss your child’s progress and to look through their individual Learning Journals... and there’s always a cup a tea or coffee available too!</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Does the nursery and pre-school accept Childcare Vouchers?</a>
			                  	   <div class="st-content">
			                  	     <p>Yes.  We accept vouchers accepts vouchers from the following Childcare Voucher companies:</p>
			                  	     <p><em>Allsave Ltd , Care4 Childcare Plus,  Childcare Vouchers,  Computershare (Busy Bees),
Co-operative Childcare, Edenred (Accor), Fair Care, Fideliti, Grass Roots Kiddivouchers, Kids Unlimited, Leap Frog, My Family Care, PES, Sodexo, Tensator, The Salary,  Exchange, You at Work</em></p>
			                  	     <p>Please <a href="/contact-details-willows-farm-day-nursery-and-preschool" title="Contact Day Nursery and Pre-School in St Albans">contact us</a> for further information if you would like to pay by Childcare Vouchers.</p>
			                  	     <p><a href="/fees-and-grants-day-nursery-preschool-st-albans" title="Willows Farm Nursery Fees and Grants">Click here</a>, for more about Government Grants for 3-5 year olds.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">What happens if my child is ill?</a>
			                  	   <div class="st-content">
			                  	     <p>If your child becomes ill during their Nursery or Pre-School day then we will contact you. 
All children will have a runny nose at some time during their Nursery and Pre-School years!  We are more than happy for the children to still attend but if your child has a communicable illness such as diarrhoea/sickness, an exclusion period will apply.</p>
									<p>Common childhood illnesses and exclusion periods are displayed at the Nursery and Pre-School.</p>
									<p>There is a hard copy guide on some of the more complex childhood diseases - if you would like a copy please ask.</p>
									<p>If your child is prescribed a medicine we are more than happy to administer it here - children need to be away from Nursery for the first 24 hours after taking it. We cannot administer non-prescribed items other than nappy creams.</p>
									<p>Should your child be unwell please let us know if they are going to be absent.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">What if my child has an accident whilst at the day nursery and pre-school?</a>
			                  	   <div class="st-content">
			                  	     <p>We have an accident policy in place at Willows Farm Day Nursery and Pre-School which all staff are briefed on. We will inform you of any accident involving your child and ask you to sign the accident form on picking up your child. In the unlikely event of a serious injury you would be advised immediately if medical attention was required.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Do you cater for dietary requirements / what is your menu?</a>
			                  	   <div class="st-content">
			                  	     <p>On application for a place at Willows Farm Day Nursery and Pre-School we ask you to provide information of dietary requirements and/or known allergies to ensure individual needs are catered for.</p>
			                  	     <p>We have a balanced menu ensuring that we meet all food groups throughout the day. Copies of our menus can be obtained onsite. For more information on our catering <a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">click here</a>.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">What does your nursery offer for children with Special Educational Needs?</a>
			                  	   <div class="st-content">
			                  	     <p>We operate a comprehensive SEN and Inclusion policy at Willows Farm Day Nursery and Pre-School. We will work collaboratively with parents and other professionals to ensure the needs of your child are met.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Can I see a copy of your nursery or pre-school terms and conditions?</a>
			                  	   <div class="st-content">
			                  	     <p>Yes of course! Simply <a href="/assets/registration-form.pdf" title="Download Registration Form" target="_blank">click here to download a PDF or our Registration Form including Terms and Conditions</a>.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Do you provide any other childcare services?</a>
			                  	   <div class="st-content">
			                  	     <p>Willows Farm Day Nursery and Pre-School is operated by Kids Play Childcare who operate Day Nursery and Pre-Schools; After School and Breakfast clubs; and Activity Day Camps during school holidays throughout the Milton Keynes area.</p>
			                  	   </div>
			                  	 </li>
			                  	 
			                  	 <li>
			                  	   <a href="#" class="trigger">Where is Willows Farm Day Nursery and Pre-School?</a>
			                  	   <div class="st-content">
			                  	     <p>You will find Willows Farm Day Nursery and Preschool just off the M25 at Junction 22, close to London Colney, St Albans, Radlett and Watford.  In fact due to our excellent location we can be reached easily from all over Hertfordshire and North London. <a href="/find-day-nursery-preschool-st-albans" title="Find Day Nursery and Preschool in St Alban">Click here for full directions</a>.</p>
			                  	   </div>
			                  	 </li>
			                  </ul>
			        		</div>
			                <!-- /Accordion -->
            
			           </div>
			           <!-- / Main -->

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>

		        	<? include("../assets/inc/contact-signpost.php") ?>

        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
        	
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>

    </body>
</html>