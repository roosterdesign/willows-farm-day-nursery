<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>OFSTED report Nursery and Preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="Willows Farm Day Nursery and Preschool is jointly operated with Kids Play Childcare who regularly achieve Good with Outstanding elements in Ofsted reports."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="brochure">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>

        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Willows Farm Village Nursery and Pre-School Brochure</h1>
        				<div class="divide head1 red"></div>
        				<p>We realise that choosing the right day nursery or pre-school is one of the most important and difficult decisions you will ever have to make.
Caring for <a href="/baby-day-nursery-st-albans" title="Baby Day Nursery in St Ablans">6 week old babies</a> through to <a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery in St Albans">Toddlers</a> and <a href="/preschool-st-albans" title="Pre-School in St Albans">5 year olds</a>, we actively work in partnership with parents and guardians to ensure healthy, happy children who are ready for mainstream school.</p>

						<h2>Nursery learning through play</h2>
						<p>We acknowledge and value children’s play and exploration as a powerful way in which they develop: through a balance of adult led and child initiated activities.</p>
						<p>To achieve this we build up a stable relationship with the children: responding sensitively to their feelings, needs and behaviour so that they become confident and feel secure. We further support children to build up friendships and to understand their own and other’s feelings through stories and group discussions.</p>
						<p>Please <a href="?" title="Download our brochure">click here</a> to download a PDF version of our brochure.</p>
						<p>Not sure where we are?  We are very close to London Colney, Radlett, St Albans and Watford. <a href="/find-day-nursery-preschool-st-albans" title="Find Day Nursery and Preschool in St Alban">Click here</a> for full directions on how to reach us at Willows Farm Nursery and Pre-School.</p>								
												
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
		        	<? include("../assets/inc/contact-signpost.php") ?>
		        	
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>
