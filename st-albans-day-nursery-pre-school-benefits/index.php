<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Why choose Nursery and Pre-School in St Albans - Willows Farm Village</title>
        <meta name = "description" content="Exceptional  brand new  Day Nursery and Pre-School at Willows Farm Village, with structured eductional play, healthy nutritious meals and Ofsted registration."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="whyus">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>A Great Choice for a Day Nursery and<br>Pre-School</h1>
        				<div class="divide head1 red"></div>
        				<p>A brand new purpose-built Nursery and Pre-School in an exclusive countryside location, Willows Farm Day Nursery and Pre-School offers contemporary facilities on an award-winning farm attraction.</p>
        				
        				<h2>Childcare from 6 weeks to 5 years</h2>
        				<p>Caring for children from <a href="/baby-day-nursery-st-albans" title="Baby Day Nursery in St Albans">6 weeks old</a> through to <a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery in St Albans">toddlers</a> and then <a href="/preschool-st-albans" title="Pre-School in St Albans">Pre-School</a> you’ll find a caring, progressive environment, professional childcare and a day packed with play and learning opportunities. Then when it’s time to rest, your children will enjoy a peaceful space for their naps.</p>
        				
        				<h3>A modern day nursery and Pre-School</h3>
        				<p>This brand new nursery has been designed to provide the layout and facilities to allow us to cater for the needs of each individual child.  Willows Farm Day Nursery and Pre-School has a baby room, pre-toddler room, toddler room and pre-school room – each designed for the age appropriate needs of the child.</p>
        				<p>Your child will enjoy planned and structured education play following the early years foundation stage curriculum. From junk modelling rockets to welly boot printing, the range of activities will keep your child active, involved and entertained during the time they spend with us.</p>

						<h4>Meet the Nursery and Pre-School Manager</h4>
						<img src="/assets/img/louise-hobbs.jpg" class="right-img">
						<p>Let us introduce you to Louise Hobbs, the Manager at Willows Farm Day Nursery and Pre-School.</p>
						<p>She has a Certificate in Early Years Practice – studied through the Open University and is planning to complete this to a full degree.</p>
						<p>As soon as you meet Louise you’ll see how passionate she is about childcare as she says 'I love my job and wouldn’t do anything else'.</p>
						<p>She is also very adventurous and has visited Uganda as a volunteer to teach English, living in a mud hut and helping to fund the important work of an orphanage.</p>
						<p>When not working with children, Louise loves spending time with family and friends and goes to the gym and swims to keep fit.</p>
						<p>Her ideal night in would be with her mum’s macaroni cheese for dinner, a Hugh Grant Film and of course a box of chocolates to round it all off!
To meet Louise <a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">click to arrange tour</a>.</p>

						<h4>Award-winning Willows Farm Village</h4>
						<p>Children have full access to all the activities in Willows Farm Village.  On a site spanning almost 500 acres there is certainly plenty of room to run around!</p>
						<p>With farmyard animals, a growing patch and acres of adventure play: the learning opportunities are exceptional.  And with their ever changing programme of seasonal events there is always something new to see and do too.  From lambing in February, Easter Egg Hunts in the spring, an A-maize-ing Maze in the summer, Potato harvesting in September and Pumpkin picking in the autumn, it’s truly childcare in the countryside.</p>
						
						<h4>Working with parents</h4>
						<p>We understand how important regular and detailed communication is so to you, so we keep you informed on your child’s developments and the milestones that they reach.</p>
						<p>To achieve this the child is allocated a designated key person with whom we encourage strong relationships are developed.  Your key person will regularly communicate with you about your child’s learning through regular feedback and Parent’s Evenings and is also available should you have any concerns.</p>
						<p>We care for our grown up visitors too!  Our parents coffee lounge offers free tea and coffee: available every morning and evening to enjoy during a conversation with staff or to pick up as a take-away for your onward journey.</p>
						
						<h4>Fresh meals prepared in our onsite kitchen</h4>
						<p>Our fees include all drinks, snacks , lunch and a two-course hot meal at the end of the day for our toddlers</p>
						<p>With an onsite kitchen, all our food is freshly prepared.  We use fresh produce to meet the children’s nutritional needs and to allow us to manage any special dietary requirements.  Have a  look at our sample menus <a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">click here</a>.</p>
						<p>Willows Farm Day Nursery and Pre-School provides home from home childcare easily accessible from London Colney, St Albans, Watford and Barnet: in fact all areas of Hertfordshire and North London.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>

		        	<? include("../assets/inc/contact-signpost.php") ?>

        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>
