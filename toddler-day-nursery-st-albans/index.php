<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Toddler Day Nursery in St Albans | Willows Farm</title>
        <meta name = "description" content="All Toddler Room activities at  St Albans based Willows Farm Day Nursery are considered  learning opportunities, with a range of fun and stimulating activities in the countryside."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="toddlers">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>

        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<hgroup>
        				  <h1>Day Nursery - Toddler Rooms</h1>
        				  <h2>From 24 to 36 months (Dependant on individual child’s needs)<br>Ratio 1:4</h2>
        				</hgroup>
        				<div class="divide head1 red"></div>
        				<p>Every activity in the Toddler Rooms is considered a learning opportunity. More structured than the Baby Room, your child will enjoy a huge and diverse range of activities which above are fun and stimulating.</p>
        				<p>Toddlers are encouraged to enjoy early maths, develop their language skills, undertake creative play and learn to relax at story-time.</p>
        				<h2>The nursery team work with you</h2>
        				<p>Your child's nominated key person will also work closely with you to mirror the potty training routine you use at home so that your child is relaxed and successful in this transition.</p>
        				<p>The Toddler Room at Willows Farm Day Nursery and Pre-School also benefits from a quiet zone for sleeping and rest.</p>
        				<h3>Access to Willows Farm Village </h3>
        				<p>When not at rest they also have the whole of Willows Farm Village to explore and we ensure that, through a structured programme, the children enjoy the huge variety of activities outdoors and undercover; with exclusive use of the facilities before opening time to the public.</p>
        				<p>When they are ready, toddler’s progress to the <a href="/preschool-st-albans" title="Pre-School in St Albans">Pre-School rooms</a>.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
        			<? include("../assets/inc/contact-signpost.php") ?>
        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
        	
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>