<div id="header-wrapper">
<div id="hero">
	<?php if ($homeslider == 'home') {print '
		<div class="slide1"></div>
		<div class="slide2"></div>
		<div class="slide3"></div>
		<div class="slide4"></div>
		<div class="slide5"></div>
		<div class="slide6"></div>
		<div class="slide7"></div>
	';}	
?>
</div>
      <header>
       	<h1 class="logo"><a href="/" title="Willows Farm Village Day Nursery and Pre-School, St Albans">Willows Farm Day Nursery &amp; Pre-School</a></h1>
       	<div class="farm-nursery"><a href="http://www.willowsfarmvillage.com/" title="Willows Farm Village" target="_blank"><img src="/assets/img/header/willows-farm-village.png" alt="Willows Farm Village"></a></div>
        <nav>
        	<ul>
        		<li><a href="/" title="Day Nursery and Pre-School in St Albans">Home page</a></li>
       			<li class="about"><a href="/about-day-nursery-and-pre-school-in-st-albans" title="About Willows Farm Village Day Nursery and Pre-School">About us</a>
       				
       				<ul>
       					<li><a href="/st-albans-day-nursery-pre-school-benefits" title="Why choose Nursery and Pre-School">Why choose us</a></li>
       					<li><a href="/FAQ-day-nursery-and-preschool-in-st-albans" title="FAQs for Day Nursery and Preschool in St Albans">FAQs</a></li>
       					<li><a href="/willows-farm-day-nursery-and-preschool" title="Willows Farm Village">Willows Farm Village</a></li>
       					<li><a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">Tours and Open Days</a></li>
       				</ul>
       			
       			</li>
   				<li><a href="/ofsted-report-st-albans-day-nursery-preschool" title="OFSTED report Nursery and Preschool in St Albans">Ofsted</a></li>
				<li class="education"><a href="/care-and-education" title="Care and Education at Willows Farm Nursery, St Albans">Education</a>
					
					<ul>
						<li><a href="/baby-day-nursery-st-albans" title="Baby Day Nursery in St Albans">Babies</a></li>
						<li><a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery in St Albans">Toddlers</a></li>
						<li><a href="/preschool-st-albans" title="Pre-School in St Albans">Pre-School</a></li>
					</ul>
				
				</li>
				<li><a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">Nutrition</a></li>
        		<li><a href="/fees-and-grants-day-nursery-preschool-st-albans" title="Willows Farm Nursery Fees and Grants">Fees</a></li>
        		<li class="contact"><a href="/contact-details-willows-farm-day-nursery-and-preschool" title="Contact Day Nursery and Pre-School in St Albans">Contact</a>
        			
        			<ul>
        				<li><a href="/find-day-nursery-preschool-st-albans" title="Find Day Nursery and Preschool in St Alban">Location</a></li>
        				<li><a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">Tours &amp; Open Days</a></li>
        			</ul>
        		
        		</li>
        	</ul>
        </nav>
        </header>
</div>