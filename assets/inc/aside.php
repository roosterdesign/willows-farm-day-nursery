<aside>
<h2>Opening Hours</h2>
<ul>
<li class="clock"><span class="wrap"><span class="big">Open Monday to Friday*<br>7:30am - 6:30pm</span><br><span class="notes">* Closed Weekends &amp; Bank Holidays</span></span></li>
<li class="mouse"><span class="wrap">Contact Email<br><script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " class='email' title='Email us'>" + linktext + "</a>")</script></span></li>
<li class="tel"><span class="wrap">Contact Phone<br><span class="big">01727 829535</span></span></li>
</ul>
<div class="divide"></div>
<h3>Visit us</h3>
<p>Book a tour to Willows Farm Day Nursery and Pre-School.</p>
<p><a href="/visit-day-nursery-and-pre-school-st-albans" class="btn blue" title="Book a tour">Book a tour</a></p>
</aside>