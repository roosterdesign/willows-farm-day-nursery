<footer>
        <div id="top-footer-wrapper">
        	<div class="layout clearfix">
        		<div class="footer1">
        			<p class="heading">Childcare in the countryside</p>
        			<ul class="left">
        				<li>Ofsted registered childcare</li>
        				<li>From 6 weeks to 5 years old</li>
        				<li>Countryside location at award winning farm attraction</li>
        				<li>Purpose built nursery</li>
        				<li>Open 7.30am to 6.30pm</li>
        				<li>Qualified and experienced staff</li>
        				<li>Regular parent meetings</li>
        			</ul>
        			<ul class="right">
        				<li>Healthy and nutritious meals made onsite</li>
        				<li>Structured educational play</li>
        				<li>Access to Willows Farm Village</li>
        				<li>Willows Members discount</li>
        				<li>Funded places available in Pre-school</li>
        				<li>All childcare vouchers accepted</li>
        			</ul>
        		</div>
        		<div class="footer2">
        			<p class="heading">Navigate our site</p>
        			<nav>
        			<ul>
        				<li><a href="/" title="Day Nursery and Pre-School in St Albans">Home page</a></li>
        				<li><a href="/about-day-nursery-and-pre-school-in-st-albans" title="About Willows Farm Village Day Nursery and Pre-School">About us</a></li>
        				<li><a href="/ofsted-report-st-albans-day-nursery-preschool" title="OFSTED report Nursery and Preschool in St Albans">Ofsted</a></li>
        				<li><a href="/care-and-education" title="Care and Education at Willows Farm Nursery, St Albans">Education</a></li>
        				<li><a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">Nutrition</a></li>
        				<li><a href="/fees-and-grants-day-nursery-preschool-st-albans" title="Willows Farm Nursery Fees and Grants">Fees</a></li>
        				<li><a href="/willows-farm-day-nursery-and-preschool" title="Willows Farm Village">Willows Farm Village</a></li>
        				<li><a href="/contact-details-willows-farm-day-nursery-and-preschool" title="Contact Day Nursery and Pre-School in St Albans">Contact</a></li>
        			</ul>
        			</nav>
        		</div>
        		<div class="footer3">
        			<p class="heading">Find us</p>
        			<img src="/assets/img/footer/map.png" alt="Find us" class="map">
        			<p><strong>Willows Farm Day<br>Nursery & Pre-School</strong><br>Coursers Road,<br>London Colney,<br>St Albans,<br>Hertfordshire,<br>AL4 0PF<br><a href="/find-day-nursery-preschool-st-albans" title="Find Day Nursery and Preschool in St Albans">View Map</a></p>

        			<p>tel: (01727) 829 535</p>
        		</div>
        </div>
</div>
<div id="bottom-footer-wrapper">
	<div class="layout">
		<p class="left">&copy; <?=date("Y");?> Willows Farm Day Nursery &amp; Pre-School</p>
        <p class="right"><a href="/terms" title="Terms and Conditions">Terms &amp; Conditions</a> | <a href="/privacy" title="Privacy Policy">Privacy</a> | <a href="/sitemap" title="Sitemap">Sitemap</a></p>
        </div>
</div>
</footer>