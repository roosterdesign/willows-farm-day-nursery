// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function noop() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());



// Place any jQuery/helper plugins in here.


// jQuery Accordion
(function( window, $, undefined ) {

	var $event = $.event, resizeTimeout;

	$event.special.smartresize 	= {
		setup: function() {
			$(this).bind( "resize", $event.special.smartresize.handler );
		},
		teardown: function() {
			$(this).unbind( "resize", $event.special.smartresize.handler );
		},
		handler: function( event, execAsap ) {
			// Save the context
			var context = this,
				args 	= arguments;

			// set correct event type
			event.type = "smartresize";

			if ( resizeTimeout ) { clearTimeout( resizeTimeout ); }
			resizeTimeout = setTimeout(function() {
				jQuery.event.handle.apply( context, args );
			}, execAsap === "execAsap"? 0 : 100 );
		}
	};

	$.fn.smartresize 			= function( fn ) {
		return fn ? this.bind( "smartresize", fn ) : this.trigger( "smartresize", ["execAsap"] );
	};
	
	$.Accordion 				= function( options, element ) {
	
		this.$el			= $( element );
		// list items
		this.$items			= this.$el.children('ul').children('li');
		// total number of items
		this.itemsCount		= this.$items.length;
		
		// initialize accordion
		this._init( options );
		
	};
	
	$.Accordion.defaults 		= {
		// index of opened item. -1 means all are closed by default.
		open			: -1,
		// if set to true, only one item can be opened. Once one item is opened, any other that is opened will be closed first
		oneOpenedItem	: false,
		// speed of the open / close item animation
		speed			: 600,
		// easing of the open / close item animation
		easing			: 'easeInOutExpo',
		// speed of the scroll to action animation
		scrollSpeed		: 900,
		// easing of the scroll to action animation
		scrollEasing	: 'easeInOutExpo'
    };
	
	$.Accordion.prototype 		= {
		_init 				: function( options ) {
			
			this.options 		= $.extend( true, {}, $.Accordion.defaults, options );
			
			// validate options
			this._validate();
			
			// current is the index of the opened item
			this.current		= this.options.open;
			
			// hide the contents so we can fade it in afterwards
			this.$items.find('div.st-content').hide();
			
			// save original height and top of each item	
			this._saveDimValues();
			
			// if we want a default opened item...
			if( this.current != -1 )
				this._toggleItem( this.$items.eq( this.current ) );
			
			// initialize the events
			this._initEvents();
			
		},
		
		
		/*_saveDimValues		: function() {
		
			this.$items.each( function() {
				
				var $item		= $(this);
				
				$item.data({
					originalHeight 	: $item.find('a:first').height(),
					offsetTop		: $item.offset().top
				});
				
			});
			
		},*/
		
		
		
		_saveDimValues    : function() {
		
			scrollOffset = this.options.scrollOffset;
			 
			 this.$items.each( function() {
			
			      var $item   = $(this);
			
			      $item.data({
			        originalHeight  : $item.find('a:first').height(),
			        offsetTop   : $item.offset().top - scrollOffset
			      });
			
			    });
			
			},


		// validate options
		_validate			: function() {
		
			// open must be between -1 and total number of items, otherwise we set it to -1
			if( this.options.open < -1 || this.options.open > this.itemsCount - 1 )
				this.options.open = -1;
	 	
		},
		_initEvents			: function() {
			
			var instance	= this;
			
			// open / close item
			this.$items.find('a:first').bind('click.accordion', function( event ) {
				
				var $item			= $(this).parent();
				
				// close any opened item if oneOpenedItem is true
				if( instance.options.oneOpenedItem && instance._isOpened() && instance.current!== $item.index() ) {
					
					instance._toggleItem( instance.$items.eq( instance.current ) );
				
				}
				
				// open / close item
				instance._toggleItem( $item );
				
				return false;
			
			});
			
			$(window).bind('smartresize.accordion', function( event ) {
				
				// reset orinal item values
				instance._saveDimValues();
			
				// reset the content's height of any item that is currently opened
				instance.$el.find('li.st-open').each( function() {
					
					var $this	= $(this);
					$this.css( 'height', $this.data( 'originalHeight' ) + $this.find('div.st-content').outerHeight( true ) );
				
				});
				
				// scroll to current
				if( instance._isOpened() )
				instance._scroll();
				
			});
			
		},
		// checks if there is any opened item
		_isOpened			: function() {
		
			return ( this.$el.find('li.st-open').length > 0 );
		
		},
		// open / close item
		_toggleItem			: function( $item ) {
			
			var $content = $item.find('div.st-content');
			
			( $item.hasClass( 'st-open' ) ) 
					
				? ( this.current = -1, $content.stop(true, true).fadeOut( this.options.speed ), $item.removeClass( 'st-open' ).stop().animate({
					height	: $item.data( 'originalHeight' )
				}, this.options.speed, this.options.easing ) )
				
				: ( this.current = $item.index(), $content.stop(true, true).fadeIn( this.options.speed ), $item.addClass( 'st-open' ).stop().animate({
					height	: $item.data( 'originalHeight' ) + $content.outerHeight( true )
				}, this.options.speed, this.options.easing ), this._scroll( this ) )
		
		},
		// scrolls to current item or last opened item if current is -1
		_scroll				: function( instance ) {
			
			var instance	= instance || this, current;
			
			( instance.current !== -1 ) ? current = instance.current : current = instance.$el.find('li.st-open:last').index();
			
			$('html, body').stop().animate({
				scrollTop	: ( instance.options.oneOpenedItem ) ? instance.$items.eq( current ).data( 'offsetTop' ) : instance.$items.eq( current ).offset().top
			}, instance.options.scrollSpeed, instance.options.scrollEasing );
		
		}
		
	};
	
	var logError 				= function( message ) {
		
		if ( this.console ) {
			
			console.error( message );
			
		}
		
	};
	
	$.fn.accordion 				= function( options ) {
	
		if ( typeof options === 'string' ) {
		
			var args = Array.prototype.slice.call( arguments, 1 );

			this.each(function() {
			
				var instance = $.data( this, 'accordion' );
				
				if ( !instance ) {
					logError( "cannot call methods on accordion prior to initialization; " +
					"attempted to call method '" + options + "'" );
					return;
				}
				
				if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {
					logError( "no such method '" + options + "' for accordion instance" );
					return;
				}
				
				instance[ options ].apply( instance, args );
			
			});
		
		} 
		else {
		
			this.each(function() {
				var instance = $.data( this, 'accordion' );
				if ( !instance ) {
					$.data( this, 'accordion', new $.Accordion( options, this ) );
				}
			});
		
		}
		
		return this;
		
	};
	
})( window, jQuery );



// jQuery Easing
// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});




// Time picker
(function(e){typeof define=="function"&&define.amd?define(["jquery"],e):e(jQuery)})(function(e){function a(t){var r=t.data("timepicker-settings"),i=t.data("timepicker-list");i&&i.length&&(i.remove(),t.data("timepicker-list",!1)),i=e("<ul />"),i.attr("tabindex",-1),i.addClass("ui-timepicker-list"),r.className&&i.addClass(r.className),i.css({display:"none",position:"absolute"}),r.minTime!==null&&r.showDuration&&i.addClass("ui-timepicker-with-duration");var s=r.durationTime!==null?r.durationTime:r.minTime,o=r.minTime!==null?r.minTime:0,u=r.maxTime!==null?r.maxTime:o+n-1;u<=o&&(u+=n);for(var a=o;a<=u;a+=r.step*60){var f=a%n,l=e("<li />");l.data("time",f),l.text(m(f,r.timeFormat));if(r.minTime!==null&&r.showDuration){var h=e("<span />");h.addClass("ui-timepicker-duration"),h.text(" ("+v(a-s)+")"),l.append(h)}i.append(l)}i.data("timepicker-input",t),t.data("timepicker-list",i);var p=r.appendTo;typeof p=="string"?p=e(p):typeof p=="function"&&(p=p(t)),p.append(i),c(t,i),i.on("click","li",function(n){t.addClass("ui-timepicker-hideme"),t[0].focus(),i.find("li").removeClass("ui-timepicker-selected"),e(this).addClass("ui-timepicker-selected"),d(t),i.hide()})}function f(){var e=new Date,t=e.getTimezoneOffset()*6e4;e.setHours(0),e.setMinutes(0),e.setSeconds(0);var n=e.getTimezoneOffset()*6e4;return new Date(e.valueOf()-n+t)}function l(t,n,r){if(!r&&r!==0)return!1;var i=t.data("timepicker-settings"),s=!1;return n.find("li").each(function(t,n){var o=e(n);if(Math.abs(o.data("time")-r)<=i.step*30)return s=o,!1}),s}function c(e,t){var n=g(e.val()),r=l(e,t,n);r&&r.addClass("ui-timepicker-selected")}function h(){if(this.value==="")return;var t=e(this),n=g(this.value);if(n===null){t.trigger("timeFormatError");return}var r=m(n,t.data("timepicker-settings").timeFormat);t.val(r)}function p(t){var n=e(this),r=n.data("timepicker-list");if(!r.is(":visible")){if(t.keyCode!=40)return!0;n.focus()}switch(t.keyCode){case 13:return d(n),u.hide.apply(this),t.preventDefault(),!1;case 38:var i=r.find(".ui-timepicker-selected");i.length?i.is(":first-child")||(i.removeClass("ui-timepicker-selected"),i.prev().addClass("ui-timepicker-selected"),i.prev().position().top<i.outerHeight()&&r.scrollTop(r.scrollTop()-i.outerHeight())):(r.children().each(function(t,n){if(e(n).position().top>0)return i=e(n),!1}),i.addClass("ui-timepicker-selected"));break;case 40:i=r.find(".ui-timepicker-selected"),i.length===0?(r.children().each(function(t,n){if(e(n).position().top>0)return i=e(n),!1}),i.addClass("ui-timepicker-selected")):i.is(":last-child")||(i.removeClass("ui-timepicker-selected"),i.next().addClass("ui-timepicker-selected"),i.next().position().top+2*i.outerHeight()>r.outerHeight()&&r.scrollTop(r.scrollTop()+i.outerHeight()));break;case 27:r.find("li").removeClass("ui-timepicker-selected"),r.hide();break;case 9:u.hide();break;case 16:case 17:case 18:case 19:case 20:case 33:case 34:case 35:case 36:case 37:case 39:case 45:return;default:r.find("li").removeClass("ui-timepicker-selected");return}}function d(e){var t=e.data("timepicker-settings"),n=e.data("timepicker-list"),r=null,i=n.find(".ui-timepicker-selected");i.length?r=i.data("time"):e.val()&&(r=g(e.val()),c(e,n));if(r!==null){var s=m(r,t.timeFormat);e.attr("value",s)}e.trigger("change").trigger("changeTime")}function v(e){var t=Math.round(e/60),n;if(t<60)n=[t,s.mins];else if(t==60)n=["1",s.hr];else{var r=(t/60).toFixed(1);s.decimal!="."&&(r=r.replace(".",s.decimal)),n=[r,s.hrs]}return n.join(" ")}function m(e,n){if(e===null)return;var r=new Date(t.valueOf()+e*1e3),i="",s,o;for(var u=0;u<n.length;u++){o=n.charAt(u);switch(o){case"a":i+=r.getHours()>11?"pm":"am";break;case"A":i+=r.getHours()>11?"PM":"AM";break;case"g":s=r.getHours()%12,i+=s===0?"12":s;break;case"G":i+=r.getHours();break;case"h":s=r.getHours()%12,s!==0&&s<10&&(s="0"+s),i+=s===0?"12":s;break;case"H":s=r.getHours(),i+=s>9?s:"0"+s;break;case"i":var a=r.getMinutes();i+=a>9?a:"0"+a;break;case"s":e=r.getSeconds(),i+=e>9?e:"0"+e;break;default:i+=o}}return i}function g(e){if(e==="")return null;if(e+0==e)return e;typeof e=="object"&&(e=e.getHours()+":"+e.getMinutes()+":"+e.getSeconds());var t=new Date(0),n=e.toLowerCase().match(/(\d{1,2})(?::(\d{1,2}))?(?::(\d{2}))?\s*([pa]?)/);if(!n)return null;var r=parseInt(n[1]*1,10),i;n[4]?r==12?i=n[4]=="p"?12:0:i=r+(n[4]=="p"?12:0):i=r;var s=n[2]*1||0,o=n[3]*1||0;return i*3600+s*60+o}var t=f(),n=86400,r="ontouchstart"in document?"touchstart":"mousedown",i={className:null,minTime:null,maxTime:null,durationTime:null,step:30,showDuration:!1,timeFormat:"g:ia",scrollDefaultNow:!1,scrollDefaultTime:!1,selectOnBlur:!1,appendTo:"body"},s={decimal:".",mins:"mins",hr:"hr",hrs:"hrs"},o=!1,u={init:function(t){return this.each(function(){var n=e(this);if(n[0].tagName=="SELECT"){var a=e("<input />"),f={type:"text",value:n.val()},l=n[0].attributes;for(var c=0;c<l.length;c++)f[l[c].nodeName]=l[c].nodeValue;a.attr(f),n.replaceWith(a),n=a}var d=e.extend({},i);t&&(d=e.extend(d,t)),d.minTime&&(d.minTime=g(d.minTime)),d.maxTime&&(d.maxTime=g(d.maxTime)),d.durationTime&&(d.durationTime=g(d.durationTime)),d.lang&&(s=e.extend(s,d.lang)),n.data("timepicker-settings",d),n.attr("autocomplete","off"),n.on("click.timepicker focus.timepicker",u.show),n.on("blur.timepicker",h),n.on("keydown.timepicker",p),n.addClass("ui-timepicker-input"),h.call(n.get(0)),o||(e("body").on(r,function(t){var n=e(t.target),r=n.closest(".ui-timepicker-input");r.length===0&&n.closest(".ui-timepicker-list").length===0&&(u.hide(),document.activeElement.blur())}),o=!0)})},show:function(t){var n=e(this),r=n.data("timepicker-list");if(n.attr("readonly"))return;if(!r||r.length===0)a(n),r=n.data("timepicker-list");if(n.hasClass("ui-timepicker-hideme")){n.removeClass("ui-timepicker-hideme"),r.hide();return}if(r.is(":visible"))return;u.hide(),n.offset().top+n.outerHeight(!0)+r.outerHeight()>e(window).height()+e(window).scrollTop()?r.css({left:n.offset().left,top:n.offset().top-r.outerHeight()}):r.css({left:n.offset().left,top:n.offset().top+n.outerHeight()}),r.show();var i=n.data("timepicker-settings"),s=r.find(".ui-timepicker-selected");s.length||(n.val()?s=l(n,r,g(n.val())):i.scrollDefaultNow?s=l(n,r,g(new Date)):i.scrollDefaultTime!==!1&&(s=l(n,r,g(i.scrollDefaultTime))));if(s&&s.length){var o=r.scrollTop()+s.position().top-s.outerHeight();r.scrollTop(o)}else r.scrollTop(0);n.trigger("showTimepicker")},hide:function(t){e(".ui-timepicker-list:visible").each(function(){var t=e(this),n=t.data("timepicker-input"),r=n.data("timepicker-settings");r&&r.selectOnBlur&&d(n),t.hide(),n.trigger("hideTimepicker")})},option:function(t,n){var r=e(this),i=r.data("timepicker-settings"),s=r.data("timepicker-list");if(typeof t=="object")i=e.extend(i,t);else if(typeof t=="string"&&typeof n!="undefined")i[t]=n;else if(typeof t=="string")return i[t];i.minTime&&(i.minTime=g(i.minTime)),i.maxTime&&(i.maxTime=g(i.maxTime)),i.durationTime&&(i.durationTime=g(i.durationTime)),r.data("timepicker-settings",i),s&&(s.remove(),r.data("timepicker-list",!1))},getSecondsFromMidnight:function(){return g(e(this).val())},getTime:function(){return new Date(t.valueOf()+g(e(this).val())*1e3)},setTime:function(t){var n=e(this),r=m(g(t),n.data("timepicker-settings").timeFormat);n.val(r)},remove:function(){var t=e(this);if(!t.hasClass("ui-timepicker-input"))return;t.removeAttr("autocomplete","off"),t.removeClass("ui-timepicker-input"),t.removeData("timepicker-settings"),t.off(".timepicker"),t.data("timepicker-list")&&t.data("timepicker-list").remove(),t.removeData("timepicker-list")}};e.fn.timepicker=function(t){if(u[t])return u[t].apply(this,Array.prototype.slice.call(arguments,1));if(typeof t=="object"||!t)return u.init.apply(this,arguments);e.error("Method "+t+" does not exist on jQuery.timepicker")}});





// Background image slider (homepage)
jQuery.extend({
    BgImageTransitions: []
});

jQuery.fn.extend({
    BgImageTransition: function(src, options) {
        if( !src ){ return jQuery; }

        //copy css from the element to the helper element function
        function copyCSS( from, to ){
            jQuery(['border-bottom-color', 'border-bottom-style', 'border-bottom-width', 'border-left-lolor',
            'border-left-style', 'border-left-width', 'border-right-color', 'border-right-style',
            'border-right-width', 'border-spacing', 'border-top-color', 'border-top-style',
            'border-top-width', 'bottom', 'height', 'left', 'margin-bottom', 'margin-left',
            'margin-right', 'margin-top', 'marker-offset', 'max-height', 'max-width', 'min-height',
            'min-width', 'opacity', 'outline-color', 'outline-offset', 'outline-width',
            'padding-bottom', 'padding-left', 'padding-right', 'padding-top', 'position', 'right',
            'top', 'z-index']).each( function(i,v){
                jQuery(to).css( v, jQuery(from).css( v ) );
            });
        }
        
        /*
         function copyCSS( from, to ){
            jQuery(['border-bottom-color', 'border-bottom-style', 'border-bottom-width', 'border-left-lolor',
            'border-left-style', 'border-left-width', 'border-right-color', 'border-right-style',
            'border-right-width', 'border-spacing', 'border-top-color', 'border-top-style',
            'border-top-width', 'bottom', 'height', 'left', 'margin-bottom', 'margin-left',
            'margin-right', 'margin-top', 'marker-offset', 'max-height', 'max-width', 'min-height',
            'min-width', 'opacity', 'outline-color', 'outline-offset', 'outline-width',
            'padding-bottom', 'padding-left', 'padding-right', 'padding-top', 'position', 'right',
            'top', 'width', 'z-index']).each( function(i,v){
                jQuery(to).css( v, jQuery(from).css( v ) );
            });
        }
        */

        //make sure there is a zIndex set - we will the helperElement to be *above* the original one
        if( !this.css('zIndex') ){
            this.css('zIndex',1);
        }

        //default plugin settings
        var settings = jQuery.extend({
            effect: {opacity: 'toggle'},
            duration: 'slow',
            easing: 'linear',
            callback: function(){},
            helperElementId: this.attr('id')+'2',
            zindex: parseInt(this.css('zIndex'),10)+1
        }, options);

        //check the bgImageTransition array and see whether there is already a helperElement for the original one. Generate one, if not
        var helperElement = null;
        if( !jQuery.BgImageTransitions[this.attr('id')] ){
            helperElement = this.clone();
            copyCSS( this, helperElement );
            helperElement.css('zIndex', settings.zindex);
            helperElement.css('display', 'none');
            helperElement.attr('id', settings.helperElementId );
            helperElement.insertAfter(this);
            jQuery.BgImageTransitions[this.attr('id')] = helperElement;
        }
        else{
            helperElement = jQuery.BgImageTransitions[this.attr('id')];
        }

        //load the image file into cache first, so that we get a nice and fast load. Make the transition when the image has been loaded in cache
        var tempImage = new Image();

        jQuery(tempImage).load( function(){
            var newImage = ( helperElement.css('display') == 'block' ) ? jQuery(this) : jQuery(helperElement);
            newImage.css('backgroundImage', 'url('+tempImage.src+')');
            helperElement.animate( settings.effect, settings.duration, settings.easing, settings.callback );
        });

        tempImage.src = src;

        return jQuery;
    }
});




