// jQuery Accordion
$(function() {
$('#st-accordion').accordion({
oneOpenedItem	: true,
scrollOffset 	: 150
});
});


// Date & time picker
$(function() {
$( "#date" ).datepicker({ dateFormat: "dd / mm / yy", minDate: 0 });
});
$('#time').timepicker();




$(document).ready(function() {

	//'Swinging' farm nursery sign in header 
	$(".farm-nursery").hover(
	function () {
	if ($(this).data("bouncing") == false || $(this).data("bouncing") == undefined){
	$(this).effect("bounce", { direction:"up", times:2, distance:3 }, 700);
	$(this).data("bouncing", true);
	}
	},
	function () {
	$(this).data("bouncing", false);
	}
	);
	
	

// Forms
	// Jump to next input when inputting DOB
    $('form #dob_d').keyup(function() {
    if(this.value.length == $(this).attr('maxlength')) {
        $('form #dob_m').focus();
    }
    });
    $('form #dob_m').keyup(function() {
    if(this.value.length == $(this).attr('maxlength')) {
        $('form #dob_y').focus();
    }
    });
    $('form#ContactForm #dob_y').keyup(function() {
    if(this.value.length == $(this).attr('maxlength')) {
        $('form #email').focus();
    }
    });
 
 
    // Main form input values
    var focusColor = { 'color': '#414143', 'font-size' : '14px', 'font-style' : 'normal'};
    var blurColor = { 'color': '#ccc', 'font-size' : '12px', 'font-style' : 'italic' };
    $("form input#childsname, form input.dob").css(blurColor);
    
    // Child Name
    var defaultChildsNameValue = "If they are currently a customer";
    var defaultChildsNameInput = $("form #childsname");
    defaultChildsNameInput.val(defaultChildsNameValue);
   	defaultChildsNameInput.focus( function() { if ($(this).val() == defaultChildsNameValue ) { $(this).val(""); $(this).css(focusColor) }; });
	defaultChildsNameInput.blur( function() { if ($(this).val() == "" ) { $(this).val(defaultChildsNameValue); $(this).css(blurColor) }; });
	
	// DOB Day
    var defaultDOBDValue = "DD";
    var defaultDOBDInput = $("form #dob_d");
    defaultDOBDInput.val(defaultDOBDValue);
   	defaultDOBDInput.focus( function() { if ($(this).val() == defaultDOBDValue ) { $(this).val(""); $(this).css(focusColor) }; });
	defaultDOBDInput.blur( function() { if ($(this).val() == "" ) { $(this).val(defaultDOBDValue); $(this).css(blurColor) }; });
	
	// DOB Month
    var defaultDOBMValue = "MM";
    var defaultDOBMInput = $("form #dob_m");
    defaultDOBMInput.val(defaultDOBMValue);
   	defaultDOBMInput.focus( function() { if ($(this).val() == defaultDOBMValue ) { $(this).val(""); $(this).css(focusColor) }; });
	defaultDOBMInput.blur( function() { if ($(this).val() == "" ) { $(this).val(defaultDOBMValue); $(this).css(blurColor) }; });
	
	// DOB Day
    var defaultDOBYValue = "YYYY";
    var defaultDOBYInput = $("form #dob_y");
    defaultDOBYInput.val(defaultDOBYValue);
   	defaultDOBYInput.focus( function() { if ($(this).val() == defaultDOBYValue ) { $(this).val(""); $(this).css(focusColor) }; });
	defaultDOBYInput.blur( function() { if ($(this).val() == "" ) { $(this).val(defaultDOBYValue); $(this).css(blurColor) }; });



	// Main form validation
	var form = $("form#contactform");
	var name = $("form#contactform #name");
	var email = $("form#contactform #email");
	var tel = $("form#contactform #tel");
	var comments = $("form#contactform #comments");

	name.blur(validateName);
	email.blur(validateEmail);
	tel.blur(validateTel);
	comments.blur(validateComments);
	
	form.submit(function(){
		if(validateName() & validateEmail() & validateTel() & validateComments())
			return true
		else 
			$('p.submit-error').slideDown('slow');
			return false;
	});
	
	
	function validateEmail(){
		var a = $(email).val();
		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
		if(filter.test(a)){
			email.removeClass("error");
			return true;
		}
		else{
			email.addClass("error");
			return false;
		}
	}
	function validateName(){
		if((name.val().length < 1)){
			name.addClass("error");
			return false;
		}
		else{
			name.removeClass("error");
			return true;
		}
	}
	function validateTel(){
		var t = $(tel).val();
		var filter = /^[0-9 ]+$/;
		if((filter.test(t))){
			tel.removeClass("error");
		return true;}
		else{
			tel.addClass("error");
			return false;
		}
	}
	function validateComments(){
		if((comments.val().length < 1)){
			comments.addClass("error");
			return false;
		}
		else{
			comments.removeClass("error");
			return true;
		}
	}
	
	
	
	
	
	
	// Open day form validation
	var form2 = $("form#opendayForm");
	var name2 = $("form#opendayForm #name");
	var email2 = $("form#opendayForm #email");

	name2.blur(validateName2);
	email2.blur(validateEmail2);
	
	form2.submit(function(){
		if(validateName2() & validateEmail2())
			return true
		else 
			$('form#opendayForm p.submit-error').slideDown('slow');
			return false;
	});
	
	
	function validateEmail2(){
		var a = $(email2).val();
		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
		if(filter.test(a)){
			email2.removeClass("error");
			return true;
		}
		else{
			email2.addClass("error");
			return false;
		}
	}
	function validateName2(){
		if((name2.val().length < 1)){
			name2.addClass("error");
			return false;
		}
		else{
			name2.removeClass("error");
			return true;
		}
	}


	
	// Book a tour form validation
	var form3 = $("form#tourForm");
	var name3 = $("form#tourForm #name2");
	var email3 = $("form#tourForm #email2");

	name3.blur(validateName3);
	email3.blur(validateEmail3);
	
	form3.submit(function(){
		if(validateName3() & validateEmail3())
			return true
		else 
			$('form#tourForm p.submit-error').slideDown('slow');
			return false;
	});
	
	
	function validateEmail3(){
		var a = $(email3).val();
		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
		if(filter.test(a)){
			email3.removeClass("error");
			return true;
		}
		else{
			email3.addClass("error");
			return false;
		}
	}
	function validateName3(){
		if((name3.val().length < 1)){
			name3.addClass("error");
			return false;
		}
		else{
			name3.removeClass("error");
			return true;
		}
	}





});





// Google map marker
function initialize() {
var latlng = new google.maps.LatLng(51.7255865, -0.2688893);
var myOptions = {
	zoom: 14,
	center: latlng,
	mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map = new google.maps.Map(document.getElementById("map_holder"),
myOptions);
var explode_image = new google.maps.MarkerImage("/assets/img/location/map-marker.png", new google.maps.Size(151,127), new google.maps.Point(0,0), new google.maps.Point(28,125));
var explode_longlat = new google.maps.LatLng(51.7255865, -0.2688893);
var explode = new google.maps.Marker({
    position: explode_longlat,
    map: map,
    icon: explode_image
});
}



	