<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Fees Day Nursery and preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="Willows Farm Day Nursery and preschool fees  include healthy meals, drinks, nappies, access to Willow Farm Village in St Albans, educational learning  and a scheduled programme of activites.   We also accept childcare vouchers and Nursery Education Grants (NEGs)."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="fees">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Day Nursery and Pre-School Fees</h1>
        				<div class="divide head1 red"></div>
        				<p>Our fees are inclusive of all healthy meals, snacks and drinks including cows milk for children of all ages.  Nappies, nappy cream, wipes and other consumables are also included.</p>
        				
        				<table class="fees">
        				 <tr>
        				  <th class="col1"></th>
        				  <th class="col2"></th>
        				  <th class="col3">Under 2 years<br>Per Day</th>
        				  <th class="col4">2 – 3 years<br>Per Day</th>
        				  <th class="col5">Over 3 Years<br>Per Day</th>
        				 </tr>
        				 <tr>
        				  <td>Morning Session<br>7.30am - 1.00pm</td>
        				  <td class="col2">Includes breakfast,<br>snack &amp; lunch</td>
        				  <td>&pound;32.00</td>
        				  <td>&pound;30.00</td>
        				  <td>&pound;28.00</td>
        				 </tr>
        				 <tr>
        				  <td>Afternoon Session<br>1.00pm – 6.30pm</td>
        				  <td class="col2">Includes snack &amp;<br>evening meal	</td>
        				  <td>&pound;32.00</td>
        				  <td>&pound;30.00</td>
        				  <td>&pound;28.00</td>
        				 </tr>
        				 <tr>
        				  <td>All Day Session<br>7.30am - 6.30pm</td>
        				  <td class="col2">Includes breakfast, snack, lunch &amp; evening meal</td>
        				  <td>&pound;52.00</td>
        				  <td>&pound;49.00</td>
        				  <td>&pound;46.00</td>
        				 </tr>
        				 <tr class="hilight">
        				  <td>Weekly Fee<br>All Day Sessions</td>
        				  <td></td>
        				  <td>&pound;260.00</td>
        				  <td>&pound;245.00</td>
        				  <td>&pound;230.00</td>
        				 </tr>
        				</table>

        				<p>Registration and deposit against your first months fees are payable.<br>Fees payable by monthly Direct Debit<br>We accept most Childcare Vouchers.</p>
        				<h2>Nursery and Pre-School discounts for Willows Farm Village Members</h2>
        				<p>As a member of Willows Farm Village you receive a 5% reduction on the under 3’s fees and 10% discount off the fee for children 3 years and over  - saving you up to almost &pound;1,200 a year</p>
        				
        				<h3>Nursery and Pre-School grants</h3>
        				<p>At Willows Farm Day Nursery and Pre-School, you can enjoy childcare from 9 am to 3.30 pm for just &pound;3, with the help of a free nursery education grant from the Government for 3 to 4 year olds.</p>
        				<p>Grants are also available against full-time places – please contact us for details by calling <strong>01727 829535</strong> or email <script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " class='email' title='Email us'>" + linktext + "</a>")</script></p>
        				
        				<h4>When can my child start free nursery education?</h4>
        				<p>Your child becomes eligible for a free Government Early Years Nursery Education Grant from the start of the school term after their third birthday. This means that most three year olds will be able to start a free Nursery or Pre-School place in the September, January or April following their third birthday.</p>
        				<p>Your child’s birthday must be before:</p>
        				<ul class="arrow">
        				  <li>31 March will be eligible for free Pre-School education from the Summer Term (April)</li>
        				  <li>31 August will be eligible for free Pre-School education from the Autumn Term (September)</li>
        				  <li>31 December will be eligible for free Pre-School education from the Spring term (January)</li>
        				</ul>
        				
        				<h4>How many funded hours can my child have at your day nursery and Pre-School</h4>
        				<p>Your Child is entitled to a maximum of 15 hours per week for 38 weeks of the year. This is split into five 3-hour sessions and by attending a lunch club at the Day Nursery, we can adjoin two sessions together in the same day. The lunch club costs &pound;5 to attend</p>
        				
        				<h4>How do I get free nursery education?</h4>
        				<p>Parents do not need to apply for the funding. Each term Willows Farm Day Nursery and Pre-School claims on behalf of the parents for the number of sessions their child will be attending and the Local Authority will then pay the funding directly to us.</p>
        				<p>The funding is not paid directly to parents. Your fees will be reduced to reflect the number of free hours you wish your child to receive.</p>
        				<p>Remember you may also be entitled to the Working Families Tax Credit to help you with the cost of childcare fees not covered by the Early Years Education Funding.</p>
        					        				
        				<h4>What do I do now?</h4>
        				<p>To obtain childcare (for 3-4 year olds) from 9 am to 3 pm from just &pound;3, simply contact us to arrange a visit by calling <strong>01727 829535</strong> or email <script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " class='email' title='Email us'>" + linktext + "</a>")</script></p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
        			<? include("../assets/inc/contact-signpost.php") ?>
        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>