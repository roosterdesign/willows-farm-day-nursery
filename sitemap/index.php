<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Sitemap | Willows Farm Day Nursery and Pre-School</title>
        <meta name = "description" content="A new Day Nursery and Pre-school at the award-winning Willows Farm Village in St Albans, Hertfordshire.  Ofsted registered childcare for babies from 6 weeks to children aged 5 years."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body>

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>

        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Sitemap</h1>
        				<div class="divide head1 red"></div>
        				<p>Select from a link below to navigate our website:</p>


						<ul class="sitemap">
			        		<li><a href="/" title="Day Nursery and Pre-School in St Albans">Homepage</a></li>
			       			<li><a href="/about-day-nursery-and-pre-school-in-st-albans" title="About Willows Farm Village Day Nursery and Pre-School">About us</a>
			       				
			       				<ul>
			       					<li><a href="/st-albans-day-nursery-pre-school-benefits" title="Why choose Nursery and Pre-School">Why choose us</a></li>
			       					<li><a href="/FAQ-day-nursery-and-preschool-in-st-albans" title="FAQs for Day Nursery and Preschool in St Albans">FAQs</a></li>
			       					<li><a href="/willows-farm-day-nursery-and-preschool" title="Willows Farm Village">Willows Farm Village</a></li>
			       					<li><a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">Tours and Open Days</a></li>
			       				</ul>
			       			
			       			</li>
			   				<li><a href="/ofsted-report-st-albans-day-nursery-preschool" title="OFSTED report Nursery and Preschool in St Albans">Ofsted</a></li>
							<li><a href="/care-and-education" title="Care and Education at Willows Farm Nursery, St Albans">Education</a>
								
								<ul>
									<li><a href="/baby-day-nursery-st-albans" title="Baby Day Nursery in St Albans">Babies</a></li>
									<li><a href="/toddler-day-nursery-st-albans" title="Toddler Day Nursery in St Albans">Toddlers</a></li>
									<li><a href="/preschool-st-albans" title="Pre-School in St Albans">Pre-School</a></li>
								</ul>
							
							</li>
							<li><a href="/healthy-meals-day-nursery-preschool-st-albans" title="Healthy meals at Day Nursery and Preschool in St Albans">Nutrition</a></li>
			        		<li><a href="/fees-and-grants-day-nursery-preschool-st-albans" title="Willows Farm Nursery Fees and Grants">Fees</a></li>
			        		<li><a href="/contact-details-willows-farm-day-nursery-and-preschool" title="Contact Day Nursery and Pre-School in St Albans">Contact</a>
			        			
			        			<ul>
			        				<li><a href="/find-day-nursery-preschool-st-albans" title="Find Day Nursery and Preschool in St Alban">Location</a></li>
			        				<li><a href="/visit-day-nursery-and-pre-school-st-albans" title="Visit Day Nursery and Pre-school in St Albans">Tours &amp; Open Days</a></li>
			        			</ul>
			        		
			        		</li>
			        	</ul>
        	
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			<? include("../assets/inc/contact-signpost.php") ?>
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>