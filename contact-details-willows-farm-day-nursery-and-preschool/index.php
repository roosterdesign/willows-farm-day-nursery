<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Contact Day Nursery and Preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="Contact Willows Farm Day Nursery and PreSchool near St Albans via the online form, by email or by telephone."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="contact">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>
        	
        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Contact the Day Nursery and Pre-School</h1>
        				<div class="divide head1 red"></div>
        				<h2>Online</h2>
						<p>Please complete our online form below and we will be back in touch within 48 hours.</p>
						
							<form method="post" action="../assets/emailer/formmail.php" name="contactform" id="contactform">
							<input type="hidden" name="env_report" value="REMOTE_HOST,REMOTE_ADDR,HTTP_USER_AGENT,AUTH_TYPE,REMOTE_USER" />
							<input type="hidden" name="recipients" value="louiseATwillowsfarmdaynursery.com" />
							<input type="hidden" name="bcc" value="neilATauburn.co.uk"/>
						    <input type="hidden" name="subject" value="Willows Farm Day Nursery - Contact Form Enquiry" />
						    <input type="hidden" name="derive_fields" value="email=email,realname=name" />
						    <input type="hidden" name="good_url" value="/thank-you" />
						    <input type="hidden" name="mail_options" value="Exclude=realname;Submit;Submit_x;Submit_y,HTMLTemplate=ContactEnquiry.htm,TemplateMissing=N/A,NoPlain,KeepLines" />

							<div class="left">
							<label>Your Name:</label>
							<input name="name" id="name" tabindex="1" />
							<label>Your Childs Name:</label>
							<input name="childsname" id="childsname" tabindex="3" />
							<label>Your E-mail:</label>
							<input name="email" id="email" tabindex="7" />
							</div>
							<div class="right">
							<label>Town you live in:</label>
							<input name="town" id="town" tabindex="2" />
							<label>Your Childs Date of Birth:</label>
							<input class="dob date" name="dob_d" id="dob_d" maxlength="2" tabindex="4" /><span>/</span><input class="dob" name="dob_m" id="dob_m" maxlength="2" tabindex="5" /><span>/</span><input class="dob year" name="dob_y" id="dob_y" maxlength="4" tabindex="6" />
							<label>Your Mobile No:</label>
							<input name="tel" id="tel" tabindex="8" />
							</div>
							<div class="clearfix"></div>
							<label>Comments:</label>
							<textarea name="comments" id="comments" cols="1" rows="1" tabindex="9"></textarea>
							<input type="submit" name="" class="submit" value="Submit form" />
							<p class="submit-error">Form not submitted, please ensure all required fields are completed correctly and try again.</p>
							<div class="clearfix"></div>
						</form>
						
						
						
						<hr class="dashed-divide">
						
						
						<div class="left">
						<h3 class="top">By Phone</h3>
						<p>Call <strong>01727 829535</strong> Monday to Friday from 7.30am – 6.30pm</p>
						</div>
						
						<div class="right">
						<h3 class="top">Email:</h3><p>Contact the Nursery Manager Louise Hobbs at <script>var username = "louise"; var hostname = "willowsfarmdaynursery.com"; var linktext = username + "@" + hostname; document.write("<a href=" + "mail" + "to:" + username + "@" + hostname + " class='email' title='Email us'>" + linktext + "</a>")</script></p>
						</div>
						
						<div class="clearfix"></div>
						<br>
						<p>Situated at junction 22 of the M25, Willows Farm Day Nursery and Pre-School is easy to get too from all over Hertfordshire and North London.  Particularly close is London Colney, St Albans, Radlett and Watford.</p>

        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
        			<? include("../assets/inc/contact-signpost.php") ?>
        			
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>

    </body>
</html>