<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>OFSTED report Nursery and Preschool in St Albans | Willows Farm</title>
        <meta name = "description" content="Willows Farm Day Nursery and Preschool is jointly operated with Kids Play Childcare who regularly achieve Good with Outstanding elements in Ofsted reports."> 
        <meta NAME="robots" CONTENT="INDEX, FOLLOW">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/assets/css/normalize.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <? include("../assets/inc/analytics.php") ?>
    </head>
    <body id="ofsted">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Page wrapper -->
        <div id="wrapper">
           	
           	<? include("../assets/inc/header.php") ?>

        	<!-- Content wrapper -->
        	<div id="content-wrapper">
        		<section class="layout">
        			<div class="main red">
        				<h1>Ofsted at Willows Farm Day Nursery<br>and Pre-School</h1>
        				<div class="divide head1 red"></div>
        				<p>Hertfordshire’s Willows Farm Day Nursery and Pre-School is jointly operated with Kids Play Childcare who have achieved Good with Outstanding elements of the Ofsted reports for the nurseries they currently operate.</p>
        				<p>The manager of Willows Farm Day Nursery and Pre-School is Louise Hobbs who previously managed the Knowhill Kids Play Nursery in Milton Keynes where she was responsible during the Ofsted inspection which achieved Outstanding grading for elements in the report.</p>
        				<p>As a new nursery we are registered with Ofsted.  The first Ofsted report will be published approximately 6 months after opening and will be available to view here.</p>
						<p>The Willows Farm Day Nursery and Pre-School is located near St Albans, Watford and Barnet.  Located just off junction 22 of the M25, it’s extremely easy to reach.</p>
        			</div>

        			<? include("../assets/inc/aside.php") ?>
        			
        			<div class="clearfix"></div>
        			
		        	<? include("../assets/inc/contact-signpost.php") ?>
		        	
        		</section>
        	</div>
        	<!-- /Content wrapper -->
        	
        	<? include("../assets/inc/footer.php") ?>
                
        </div>
        <!-- /Page Wrapper -->

    <? include("../assets/inc/global.js.php") ?>
    </body>
</html>
